package main

import (
	"fmt"
	"database/sql"
	"golang.org/x/crypto/bcrypt"
	"encoding/json"
	"net/http"
	_ "github.com/lib/pq"
	"time"
)

// Create a struct that models the structure of a user, both in the request body, and in the DB
type Credentials struct {
	Password string `json:"password", db:"password"`
	Username string `json:"username", db:"username"`
}

type User struct {
	Id int
	Username string
	CreatedAt string
	UpdatedAt string
}

type allUsers []User

type error interface {
    Error() string
}

type position struct {
	ID          int `json:id`
	OrderId       string `json:"order_id"`
	BuyPrice float64 `json:"buy_price"`
	SellPrice float64 `json:"sell_price"`
	Status string `json:"status"`
	CreatedAt string `json:"created_at"` 
	UpdatedAt string `json:"updated_at"` 
	TradingSymbol string `json:"trading_symbol"` 
	Qty int `json:"qty"`
	TradingType string `json:"trading_type"`
	LegType string `json:"leg_type"`
	Expiry string `json:"expiry"`
	Strike int `json:"strike"`
}

type allPositions []position

func homeLink(w http.ResponseWriter, r *http.Request) {
	 fmt.Fprintf(w, "Welcome home!")
}

func Signup(w http.ResponseWriter, r *http.Request){
	// Parse and decode the request body into a new `Credentials` instance
	creds := &Credentials{}
	err := json.NewDecoder(r.Body).Decode(creds)
	if err != nil {
		// If there is something wrong with the request body, return a 400 status
		w.WriteHeader(http.StatusBadRequest)
		return 
	}
	fmt.Println("err ", err)
	// Salt and hash the password using the bcrypt algorithm
	// The second argument is the cost of hashing, which we arbitrarily set as 8 (this value can be more or less, depending on the computing power you wish to utilize)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(creds.Password), 8)
	// Next, insert the username, along with the hashed password into the database
	if _, err = db.Query("insert into users(username, password) values ($1, $2)", creds.Username, string(hashedPassword)); err != nil {
		fmt.Println("err **** ", err)
		// If there is any issue with inserting into the database, return a 500 error
		w.WriteHeader(403)
		w.Header().Set("Content-Type", "application/json")
		resp := make(map[string]string)
		resp["message"] = err.Error()
		jsonResp, err := json.Marshal(resp)
		if err != nil {
			fmt.Println("Error happened in JSON marshal. Err: %s", err)
		}
		w.Write(jsonResp)
		return
	}
		w.Header().Set("Content-Type", "application/json")
		resp := make(map[string]string)
		resp["data"] = "User Created Successfully!"
		jsonResp, err := json.Marshal(resp)
		if err != nil {
			fmt.Println("Error happened in JSON marshal. Err: %s", err)
		}
		w.Write(jsonResp)
}

func Signin(w http.ResponseWriter, r *http.Request){
	// Parse and decode the request body into a new `Credentials` instance	
	creds := &Credentials{}
	user := &User{}
	err := json.NewDecoder(r.Body).Decode(creds)
	if err != nil {
		// If there is something wrong with the request body, return a 400 status		
		w.WriteHeader(http.StatusBadRequest)
		return 
	}
	// Get the existing entry present in the database for the given username
	result := db.QueryRow("select id, username, password, created_at, updated_at from users where username=$1", creds.Username)
	if err != nil {
		// If there is an issue with the database, return a 500 error
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// We create another instance of `Credentials` to store the credentials we get from the database
	storedCreds := &Credentials{}
	// Store the obtained password in `storedCreds`
	err = result.Scan(&user.Id,&user.Username, &storedCreds.Password, &user.CreatedAt, &user.UpdatedAt)
	fmt.Println("data",storedCreds, result, err, user)
	if err != nil {
		// If an entry with the username does not exist, send an "Unauthorized"(401) status
		if err == sql.ErrNoRows {
			w.WriteHeader(http.StatusUnauthorized)
			w.Header().Set("Content-Type", "application/json")
			resp := make(map[string]string)
			resp["message"] = err.Error()
			jsonResp, _ := json.Marshal(resp)
			w.Write(jsonResp)
			return
		}
		// If the error is of any other type, send a 500 status
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Compare the stored hashed password, with the hashed version of the password that was received
	if err = bcrypt.CompareHashAndPassword([]byte(storedCreds.Password), []byte(creds.Password)); err != nil {
		// If the two passwords don't match, return a 401 status
		w.WriteHeader(http.StatusUnauthorized)
		w.Header().Set("Content-Type", "application/json")
		resp := make(map[string]string)
		resp["message"] = err.Error()
		jsonResp, _ := json.Marshal(resp)
		w.Write(jsonResp)
	}
	// If we reach this point, that means the users password was correct, and that they are authorized
	// The default 200 status is sent
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}

func getUser(w http.ResponseWriter, r *http.Request) {
	username := r.URL.Query().Get("username")
	var user = User{}
	fmt.Println("username ", username)
	rows, err := db.Query(`SELECT id, username, created_at, updated_at FROM users where username=$1`, username)
	CheckError(err)
	for rows.Next() {
		err = rows.Scan(&user.Id, &user.Username, &user.CreatedAt, &user.UpdatedAt)
		CheckError(err)
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}

func getAllUsers(w http.ResponseWriter, r *http.Request) {
	var users = allUsers{}
	var user = User{}
	rows, err := db.Query(`SELECT id, username, created_at, updated_at FROM users`)
	CheckError(err)
	for rows.Next() {
		err = rows.Scan(&user.Id, &user.Username, &user.CreatedAt, &user.UpdatedAt)
		CheckError(err)
		fmt.Println("user", user)
		users = append(users, user)
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}

func getAllPositions(w http.ResponseWriter, r *http.Request) {
	var positions = allPositions{}
	expiry := r.URL.Query().Get("expiry")
	query := "SELECT * FROM position"
	if expiry != "" {
		query = fmt.Sprintf("SELECT * FROM position where expiry >='%s'", expiry)
	}
	fmt.Println("expiry ", expiry, time.Now(), query)
	rows, err := db.Query(query)
	CheckError(err)
	for rows.Next() {
		var order_id, status, created_at, updated_at, trading_symbol, trading_type, leg_type, expiry  string
		var id, qty, strike int
		var buy_price, sell_price float64
		err = rows.Scan(&id, &order_id, &buy_price, &sell_price, &status, &created_at, &updated_at, &trading_symbol, &qty, &trading_type, &leg_type, &expiry, &strike)
		CheckError(err)
		msg := position{
			ID: id,
			OrderId: order_id,
			BuyPrice: buy_price,
			SellPrice: sell_price,
			Status: status,
			CreatedAt: created_at,
			UpdatedAt: updated_at,
			TradingSymbol: trading_symbol,
			Qty: qty,
			TradingType: trading_type,
			LegType: leg_type,
			Expiry: expiry,
			Strike: strike,
		}
		positions = append(positions, msg)
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(positions)
}

func CheckError(err error) {
    if err != nil {
		fmt.Println("err !", err)
        panic(err)
    }
}