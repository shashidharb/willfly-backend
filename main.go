package main

import (
	"database/sql"
	"net/http"
	"log"
	_ "github.com/lib/pq"
	"fmt"
)

const (
    host     = "localhost"
    port     = 5432
    user     = "postgres"
    password = "asterisk"
    dbname   = "willfly"
)

const hashCost = 8
var db *sql.DB

func main() {
	http.HandleFunc("/", homeLink)
	http.HandleFunc("/signin", Signin)
	http.HandleFunc("/signup", Signup)
	http.HandleFunc("/users", getAllUsers)
	http.HandleFunc("/user", getUser)
	http.HandleFunc("/positions", getAllPositions)
	// initialize our database connection
	initDB()
	// start the server on port 8000
	log.Fatal(http.ListenAndServe(":8000", nil))
}

func initDB(){
	var err error
	// Connect to the postgres db
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)	
	db, err = sql.Open("postgres", psqlconn)
	if err != nil {
		panic(err)
	}
}