CREATE TABLE position (
	id serial4 NOT NULL,
	order_id varchar(200) NOT NULL,
	buy_price float8 NULL DEFAULT 0.0,
	sell_price float8 NULL DEFAULT 0.0,
	status varchar(100) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	trading_symbol varchar(200) NOT NULL,
	qty int4 NOT NULL DEFAULT 0,
	trading_type varchar(100) NULL,
	leg_type varchar(100) NULL,
	expiry TIMESTAMP NOT NULL,
	strike int4 NOT NULL
);