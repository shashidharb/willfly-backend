CREATE TABLE instruments (
	id serial4 NOT NULL,
	trading_symbol VARCHAR(100) NOT NULL,
	instrument_token VARCHAR(50) NULL,
	expiry VARCHAR(100) NULL,
	strike int4 NULL,
	lot_size int4 NULL,
	instrument_type VARCHAR(50) NULL,
	segment VARCHAR(50) NULL,
	exchange VARCHAR(30) NULL
);