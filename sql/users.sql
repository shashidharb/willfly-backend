
create table users (
  id serial PRIMARY KEY,
  username VARCHAR ( 300 ) UNIQUE NOT NULL,
  password VARCHAR ( 100 ) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);


